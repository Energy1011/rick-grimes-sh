#!/bin/bash

# Kill zombie process,
# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ZOMBIES="$(ps -A -ostat| grep -e '^[Zz]' | wc -l)"
TOLERANCE=10

if [ "$ZOMBIES" -gt 0 ]; then
		#echo "Zombies found:" $ZOMBIES;
        #echo "Rick has found zombies";
        kill -HUP $(ps -A -ostat,ppid | grep -e '[zZ]'| awk '{ print $2 }')
        if [ "$ZOMBIES" -gt $TOLERANCE ]; then
                EMAIL="<your_mail@here.com>"
                EMAILMESSAGE="RickGrimes.sh Zombies : $ZOMBIES Killed";
                SUBJECT="RickCrimes.sh";
                echo "$EMAILMESSAGE" | mail -s "$SUBJECT" $EMAIL
        fi
fi
exit;
